#pragma once

#include "SDLCommon.h"

class SDLGame
{
public:
	SDLGame();
	~SDLGame();

	void init();
	void run();
};

