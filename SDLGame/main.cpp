#include "SDLGame.h"

int main(int argv, char** args) {
	SDLGame* game = new SDLGame();

	game->init();
	game->run();

	return 0;
}